# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    I18n.locale = get_locale_from_header(env)
    @status, @headers, @response = @app.call(env)
    @headers['Content-Language'] = I18n.locale.to_s
    [@status, @headers, @response]
  end

  private

  def get_locale_from_header(env)
    if env['HTTP_ACCEPT_LANGUAGE'].nil?
      I18n.default_locale
    else
      client_locale = env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      I18n.available_locales.map(&:to_s).include?(client_locale) ? client_locale : I18n.default_locale
    end
  end
  # END
end
