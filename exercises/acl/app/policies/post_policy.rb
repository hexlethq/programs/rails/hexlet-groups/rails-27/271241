# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user&.present?
  end

  def update?
    record&.user_id == user&.id || user&.admin?
  end

  def destroy?
    user&.admin?
  end
  # END
end
