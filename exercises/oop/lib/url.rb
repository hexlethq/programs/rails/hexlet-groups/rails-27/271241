# frozen_string_literal: true

# BEGIN
require 'forwardable'
require 'uri'

# Class for Url
class Url
  extend Forwardable
  include Comparable

  attr_reader :url

  def <=>(other)
    url.to_s <=> other.url.to_s
  end

  def initialize(url)
    @url = URI(url)
  end

  def_delegators :url, :scheme, :host

  def query_params
    url.query.split('&').each_with_object({}) do |pair, result|
      key, value = pair.split('=')
      result[key.to_sym] = value
    end
  end

  def query_param(key, default_value = nil)
    query_params.key?(key) ? query_params[key] : default_value
  end
end
# END
