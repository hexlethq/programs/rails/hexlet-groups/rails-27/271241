# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
    @test_array = [1, 2, 3]
    @constructed_stack = Stack.new(@test_array)
  end

  def teardown; end

  def test_empty_stack
    assert_empty @stack
    assert_instance_of Array, @stack.to_a
    assert_empty @stack.to_a
    assert_equal 0, @stack.size
  end

  def test_constructor
    assert_equal @test_array, @constructed_stack.to_a
    assert_equal 3, @constructed_stack.size
    refute @constructed_stack.empty?
  end

  def test_push
    @stack.push! 1
    @stack.push! 2

    assert_equal [1, 2], @stack.to_a
  end

  def test_pop
    elem1 = @constructed_stack.pop!
    elem2 = @constructed_stack.pop!

    assert_equal 3, elem1
    assert_equal 2, elem2
    assert_equal [1], @constructed_stack.to_a
  end

  def test_clear
    @constructed_stack.clear!
    assert_instance_of Array, @constructed_stack.to_a
    assert_empty @constructed_stack.to_a
    assert_equal 0, @constructed_stack.size
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
