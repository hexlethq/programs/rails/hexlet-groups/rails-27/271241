# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  return arr if arr.empty?

  parity = arr[0] % 2
  arr.select { |num| num % 2 == parity }
end
# END
