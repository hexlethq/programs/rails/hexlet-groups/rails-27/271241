# frozen_string_literal: true

# BEGIN
def anagramm_filter(sample, words)
  sample_letters = letter_count(sample)
  words.select { |word| letter_count(word) == sample_letters }
end

def letter_count(word)
  word.chars.each_with_object({}) do |letter, count|
    count[letter] ||= 0
    count[letter] += 1
  end
end
# END
