# frozen_string_literal: true

# BEGIN
def count_by_years(hash)
  return hash if hash.empty?

  hash.select { |user| user[:gender] == 'male' }.each_with_object({}) do |user, count|
    year = Time.new(user[:birthday]).year.to_s
    count[year] ||= 0
    count[year] += 1
  end
end
# END
