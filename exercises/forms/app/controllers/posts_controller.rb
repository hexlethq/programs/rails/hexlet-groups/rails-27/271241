# frozen_string_literal: true

class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def show
    @post = Post.find params[:id]
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new post_params
    if @post.save
      redirect_to post_path(@post), notice: 'Post has been created'
    else
      flash[:alert] = 'Post has not been created'
      render :new
    end
  end

  def edit
    @post = Post.find params[:id]
  end

  def update
    @post = Post.find params[:id]
    if @post.update post_params
      redirect_to post_path(@post), notice: 'Post has been updated'
    else
      flash[:alert] = 'Post has not been updated'
      render :edit
    end
  end

  def destroy
    @post = Post.find params[:id]
    if @post.destroy
      redirect_to posts_path, notice: 'Task has been removed'
    else
      redirect_to post_path(@post), alert: 'Task has not been removed'
    end
  end

  private

  def post_params
    params.require(:post).permit(:title,
                                 :body,
                                 :summary,
                                 :published)
  end
end
