# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :require_auth_user, only: %i[show]
  before_action only: %i[show] do
    flash[:error] = t('.require_activation') unless current_user.active?
  end

  def show
    @user = current_user
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      @user.send_activation_email

      sign_in @user
      redirect_to @user, notice: t('.waiting_confirmation')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def confirm
    redirect_to :new unless params[:confirmation_token]

    # BEGIN
    @user = User.find_by(confirmation_token: params[:confirmation_token])

    return redirect_to root_url, alert: t('.activate_problems') unless @user

    if @user.activate!
      redirect_to user_url(@user), notice: t('.activated')
    else
      redirect_to root_url, alert: t('.activate_problems')
    end
    # END
  end

  private

  def require_auth_user
    redirect_to new_user_path unless current_user
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(
      :name,
      :email,
      :password,
      :password_confirmation
    )
  end
end
