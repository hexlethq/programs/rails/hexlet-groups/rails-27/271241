# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'Import users from csv'
  task :import_users, [:path] => :environment do |_task, args|
    if args[:path].nil?
      puts 'No path specified'
      exit
    end

    users = CSV.read(args[:path], headers: true)

    imported_count = 0

    users.map(&:to_h).each do |row|
      next if User.find_by(email: row['email'])

      User.create(row)
      imported_count += 1
    end

    puts "Users imported: #{imported_count}"
  end
end
