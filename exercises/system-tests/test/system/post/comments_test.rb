# frozen_string_literal: true

require 'application_system_test_case'

class Post::CommentsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    @comment = @post.comments
  end

  test 'creating a Comment' do
    visit post_path @post

    fill_in 'post_comment_body', with: 'Creating a Comment'
    click_on 'Create Comment'

    assert_text 'Comment was successfully created.'
    click_on 'Back'
  end

  test 'updating a Comment' do
    visit edit_post_comment_path @post, @comment

    fill_in 'Body', with: 'Updating a Comment'
    click_on 'Update Comment'

    assert_text 'Comment was successfully updated.'
    click_on 'Back'
  end

  test 'destroying a Comment' do
    visit post_path @post

    page.accept_confirm do
      click_on 'Delete', match: :first
    end

    click_on 'Back'
  end
end
