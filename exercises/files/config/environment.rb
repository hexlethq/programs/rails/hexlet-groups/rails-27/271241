# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

my_cfg = File.join(Rails.root, 'config', 'my_cfg.rb')
load(my_cfg) if File.exist?(my_cfg)

# Initialize the Rails application.
Rails.application.initialize!
