# frozen_string_literal: true

# BEGIN
def fibonacci(count)
  return nil if count.negative?
  return 0 if count == 1

  num = 0
  num2 = 1
  2.upto(count) do
    tmp = num2
    num2 += num
    num = tmp
  end
  num
end
# END
