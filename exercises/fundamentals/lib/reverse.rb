# frozen_string_literal: true

# BEGIN
def reverse(word)
  last_sym = word.length - 1
  str = ''
  last_sym.downto(0) do |n|
    str += word[n]
  end
  str
end
# END
