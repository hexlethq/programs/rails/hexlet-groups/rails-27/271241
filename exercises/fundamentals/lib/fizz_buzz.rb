# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  arr = []
  start.upto(stop) do |num|
    arr << if (num % 3).zero? && (num % 5).zero?
             'FizzBuzz'
           elsif (num % 3).zero?
             'Fizz'
           elsif (num % 5).zero?
             'Buzz'
           else
             num.to_s
           end
  end
  arr.join(' ')
end
# END
