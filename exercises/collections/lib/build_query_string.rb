# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  query = []
  params.sort.each do |key, value|
    query << "#{key}=#{value}"
  end
  query.join('&')
end
# END
