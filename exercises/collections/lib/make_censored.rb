# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  result = []
  text.split.each do |word|
    result.append stop_words.include?(word) ? '$#%!' : word
  end
  result.join(' ')
  # END
end
