# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_users_default_value
    user1 = User.new name: 'Lancelot', active: 'Yes'
    assert_equal 'Lancelot', user1.name
    assert_equal nil, user1.birthday
    assert_equal true, user1.active

    user2 = User.new
    assert_equal 'Andrey', user2.name
    assert_equal false, user2.active
  end
  # END
end
