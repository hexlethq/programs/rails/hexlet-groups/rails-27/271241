# frozen_string_literal: true

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get bulletins_url
    assert_response :success
  end

  test 'should get published showpage' do
    get bulletin_url bulletins(:published)
    assert_response :success
  end

  test 'should get unpublished showpage' do
    get bulletin_url bulletins(:draft)
    assert_response :success
  end
end
