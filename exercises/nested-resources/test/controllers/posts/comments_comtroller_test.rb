# frozen_string_literal: true

require 'test_helper'

class Post::CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = post_comments(:one)
    @post = @comment.post
  end

  test 'should create comment' do
    assert_difference('Post::Comment.count') do
      post post_comments_url(@post), params: { post_comment: {
        body: 'Body'
      } }
    end

    assert_redirected_to post_url(@post)
  end

  test 'should get edit' do
    get edit_post_comment_url(@post, @comment)
    assert_response :success
  end

  test 'should update comment' do
    patch post_comment_url(@post, @comment), params: { post_comment: {
      body: 'Another Body'
    } }

    assert_equal @comment.reload.body, 'Another Body'
    assert_redirected_to post_url(@post)
  end

  test 'should destroy post' do
    assert_difference('Post::Comment.count', -1) do
      delete post_comment_url(@post, @comment)
    end

    assert_redirected_to post_url(@post)
  end
end
