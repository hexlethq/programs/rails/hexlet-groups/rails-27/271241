# frozen_string_literal: true

class Router
  def call(env)
    request = Rack::Request.new(env)
    status = 200
    headers = {}

    case request.path
    when '/'
      body = 'Hello, World!'
    when '/about'
      body = 'About page'
    else
      status = 404
      body = '404 Not Found'
    end

    [status, headers, body]
  end
end
