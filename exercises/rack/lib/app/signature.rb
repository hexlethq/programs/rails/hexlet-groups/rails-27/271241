# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    puts('jojo')
    status, headers, body = @appl.call(env)
    hash = Digest::SHA256.hexdigest body
    body_with_hash = "#{body}\n#{hash}"
    [status, headers, body_with_hash]
  end
end
