# frozen_string_literal: true

require 'digest'

class ExecutionTimer
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    start = Time.now
    status, headers, body = @appl.call(env)
    stop = Time.now
    time = "Response Time: #{stop - start}"
    body_with_time = "#{body}\n#{time}\n"
    [status, headers, body_with_time]
  end
end
