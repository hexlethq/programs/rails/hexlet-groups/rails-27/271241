# frozen_string_literal: true

class AdminPolicy
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    request = Rack::Request.new(env)
    status, headers, body = if request.path.start_with? '/admin'
                              [403, {}, '']
                            else
                              @appl.call(env)
                            end
    [status, headers, body]
  end
end
