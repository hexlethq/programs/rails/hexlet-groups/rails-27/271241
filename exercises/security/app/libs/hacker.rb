# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      sign_up_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users/sign_up')
      users_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users')

      response = Net::HTTP.get_response(sign_up_uri)

      token = Nokogiri::HTML(response.body).at('input[name="authenticity_token"]')['value']
      cookie = response['set-cookie']

      headers = { cookie: cookie }
      params = {
        'user[email]': email,
        'user[password]': password,
        'user[password_confirmation]': password,
        authenticity_token: token
      }

      Net::HTTP.post(users_uri, URI.encode_www_form(params), headers)
      # END
    end
  end
end
