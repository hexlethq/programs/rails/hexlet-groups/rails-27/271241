# frozen_string_literal: true

class TasksController < ApplicationController
  def index
    @tasks = Task.all
  end

  def show
    @task = Task.find params[:id]
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new task_params
    if @task.save
      redirect_to task_path(@task), notice: 'Task has been created'
    else
      flash[:alert] = 'Task has not been created'
      render :new
    end
  end

  def edit
    @task = Task.find params[:id]
  end

  def update
    @task = Task.find params[:id]
    if @task.update task_params
      redirect_to task_path(@task), notice: 'Task has been updated'
    else
      flash[:alert] = 'Task has not been updated'
      render :edit
    end
  end

  def destroy
    @task = Task.find params[:id]
    if @task.destroy
      redirect_to tasks_path, notice: 'Task has been removed'
    else
      redirect_to task_path(@task), alert: 'Task has not been removed'
    end
  end

  private

  def task_params
    params.require(:task).permit(:name,
                                 :description,
                                 :status,
                                 :creator,
                                 :performer,
                                 :completed)
  end
end
