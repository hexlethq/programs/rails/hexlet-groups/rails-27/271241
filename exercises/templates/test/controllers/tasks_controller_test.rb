# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'should_get_index' do
    get tasks_path
    assert_response :success
  end

  test 'should_get_show' do
    get task_path(tasks(:one))
    assert_response :success
  end

  test 'should_get_new' do
    get new_task_path
    assert_response :success
  end

  test 'should_get_edit' do
    get edit_task_path(tasks(:one))
    assert_response :success
  end

  test 'should_create_task' do
    attrs = {
      name: 'testTask',
      description: 'testDescription',
      status: 'new',
      creator: 'qew',
      performer: 'weq',
      completed: false
    }

    post tasks_path, params: { task: attrs }

    assert_not_nil Task.find_by(name: 'testTask')
    assert_redirected_to task_path(Task.last)
    assert_equal 'Task has been created', flash[:notice]
  end

  test 'should_not_create_task' do
    attrs = {
      name: 'testTask'
    }

    post tasks_path, params: { task: attrs }

    assert_nil Task.find_by(name: 'testTask')
    assert_equal 'Task has not been created', flash[:alert]
  end

  test 'should_update_task' do
    attrs = {
      name: 'changedName',
      description: 'MyText',
      status: 'MyString',
      creator: 'MyString',
      performer: 'MyString',
      completed: false
    }

    patch task_path(tasks(:one)), params: { task: attrs }

    assert_not_nil Task.find_by(name: 'changedName')
    assert_redirected_to task_path(tasks(:one))
    assert_equal 'Task has been updated', flash[:notice]
  end

  test 'should_destroy_task' do
    delete task_path(tasks(:two))

    assert_nil Task.find_by(name: 'DeletedString')
    assert_redirected_to tasks_path
    assert_equal 'Task has been removed', flash[:notice]
  end
end
